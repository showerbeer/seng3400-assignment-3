/**
 * SENG3400 Assignment 3
 * Author: 	Christian Lassen
 * Sd.no:  	3121707
 * Email:	christian.lassen@uon.edu.au
*/

import SyncApp.*;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;
import org.omg.CORBA.*;
import org.omg.PortableServer.*;   
import org.omg.PortableServer.POA;

/*
 * SyncCallbackServant
 * Provides an interface for the server to respond to the client with an asynchronous
 * callback.
*/
class SyncCallbackServant extends SyncCallbackPOA
{
	private ORB orb;

	// Set orb object
	public void setORB(ORB _orb)
	{
		this.orb = _orb;
	}

	// callback provides a method for the server to respons with a callback
	public void callback(int val)
	{
		// Calculate total wait time
		SyncClient.update(val);
		SyncClient.stopWaiting();
		System.out.println("Received callback with value: " + val);
	}
}

/**
 * SyncClient class
 * This client runs two methods, deferred or asynchronous, depending the run time parameter.
 * Server hostname, port are both supplied at runtime. See Readme
 * The SyncServer must be running on the target hostname.
*/
public class SyncClient
{
	// Instance variables
	private static int SLEEP_TIME = 750; // How long the client sleeps for inbetween printout
	private static int value, counter; // The private value and the counter to be displayed
	private static String type; // either "deferred" or "synchronous", supplied as run parameter
	private static boolean updated; // Whether or not the value has been changed or not
	private static long startWaitTime, stopWaitTime, totalWaitTime;
	
	// Main method.
	public static void main(String[] args)
	{
		// Set starting values
		value = 200;
		counter = 1;
		updated = false;
		type = args[args.length-1].toUpperCase(); // the type of request, deferred or asynchronous
		
		// Switch on the type of request specified at runtime
		switch(type) {
			case "DEFERRED":
				deferred(args);
				break;
			case "ASYNCHRONOUS":
				async(args);
				break;

			default:
				System.out.println("Please use a valid run parameter for type of request (\"deferred\" or \"asynchronous\")");
		}

		// Program has finished
		System.out.println("Client exiting");
	}

	/**
	 * Deferred synchronous
	 * This type of request issues a deferred request for a new integer value,
	 * then continues to run for another 5 steps.
	 * If the server has not responded with a new randomly generated number by then,
	 * the client blocks and waits for a response. Upon receving the new value, the
	 * client continues.
	 *
	 * @param args An array of runtime parameters which are passed forward from main().
	*/
	public static void deferred(String[] args) {
		try {
			// Initialse the ORB
			ORB orb = ORB.init(args, null);
			org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
			
			// Narrow the context type and set path
			NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
			NameComponent nc = new NameComponent("Sync", "");
			NameComponent[] path = {nc};

			// Initialise the rando numbe generator and narrow
			RNG RNGRef = RNGHelper.narrow(ncRef.resolve(path));
			
			// Create Request object and specify return type
			// In Java IDL, long is used for integer
			Request r = RNGRef._request("getRandomNumber");
			r.set_return_type(orb.get_primitive_tc(TCKind.tk_long));
 			
 			// Print counter and value 5 times
			for (int i=0; i<5; i++) {
			 	System.out.format("Counter: %d\tValue: %d\n", counter, value);
			 	Thread.sleep(SLEEP_TIME);
			 	counter++;
		 	}

		 	// Make call to server, non-blocking
			r.send_deferred();
			System.out.println("Sent deferred request to server");

			// Print counter and value 5 times
			for (int i=0; i<5; i++) {
			 	System.out.format("Counter: %d\tValue: %d\n", counter, value);
			 	Thread.sleep(SLEEP_TIME);
			 	counter++;
		 	}

		 	// Specify when we started waiting
		 	startWaiting();

			// Block while waiting
			r.get_response();

			// get_response no longer blocking, calculate wait time
			stopWaiting();
			double waited = getWaitTime();
			System.out.format("Responce received. Waited %.2fs\n", waited);


			// Update the value with returned generated number
			// Set client as updated
			value = r.return_value().extract_long();
			
			// Print counter and value 5 times
			for (int i=0; i<5; i++) {
			 	System.out.format("Counter: %d\tValue: %d\n", counter, value);
			 	Thread.sleep(SLEEP_TIME);
			 	counter++;
		 	}
	 	} catch(Exception e) {
			System.out.println("Error: " + e);
		}
	}

	/**
	 * Asynchronous
	 * This type of request issues a request for a new integer value, then continues
	 * to run indefinitely until the server responds with a new value.
	 * When the server does respons with a new value, the client executes for another
	 * five times before exiting.
	 *
	 * @param args An array of runtime parameters which are passed forward from main().
	*/
	public static void async(String[] args) {
		try {

			// Initialise the ORB
			ORB orb = ORB.init(args, null);
			org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
			POA rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
			rootpoa.the_POAManager().activate();
			
			// Narrow the context type and set path
			NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef); // Cast to NamingContext
			NameComponent nc = new NameComponent("Sync", "");
			NameComponent[] path = {nc};

			// Initialise the rando numbe generator and narrow
			RNG RNGRef = RNGHelper.narrow(ncRef.resolve(path));
			
			// Callback stuff
			SyncCallbackServant syncCallbackImpl = new SyncCallbackServant();
			syncCallbackImpl.setORB(orb);
			
			org.omg.CORBA.Object ref = rootpoa.servant_to_reference(syncCallbackImpl);
			SyncCallback rref = SyncCallbackHelper.narrow(ref);

			// Create Request object and pass a reference for the client to the server
			Request r = RNGRef._request("getRandomNumberAsync");
			Any param = r.add_in_arg();
			param.insert_Object(rref);

			// Run five times
			for(int i=0; i<5; i++) {
				System.out.format("Counter: %d\tValue: %d\n", counter, value);
				Thread.sleep(SLEEP_TIME);
				counter++;
			}

			// Send the request
			System.out.println("Sending asynchronous request");
			r.send_deferred();
			startWaiting();

			while(!updated) {
				System.out.format("Counter: %d\tValue: %d\n", counter, value);
				Thread.sleep(SLEEP_TIME);
				counter++;
			}

			double waited = SyncClient.getWaitTime();
			System.out.format("Responce received. Waited %.2fs\n", waited);

			for(int i=0; i<5; i++) {
				System.out.format("Counter: %d\tValue: %d\n", counter, value);
				Thread.sleep(SLEEP_TIME);
				counter++;
			}

		} catch (Exception e) {
			System.out.println("Error: " + e);
		}
	}

	public static void update(int newVal) {
		SyncClient.value = newVal;
		SyncClient.updated = true;
	}

	public static void startWaiting() {
		startWaitTime = System.currentTimeMillis();
	}

	public static void stopWaiting() {
		stopWaitTime = System.currentTimeMillis();
		totalWaitTime = stopWaitTime- startWaitTime;
	}

	public static double getWaitTime() {
		return (double) totalWaitTime / 1000;
	}
}