/**
 * SENG3400 Assignment 3
 * Author: 	Christian Lassen
 * Sd.no:  	3121707
 * Email:	christian.lassen@uon.edu.au
*/

import SyncApp.*; // The skeleton and interfaces
import org.omg.CosNaming.*; // The naming service
import org.omg.CosNaming.NamingContextPackage.*; // The special name service exception package
import org.omg.CORBA.*; // Needed by all CORBA applications
import org.omg.PortableServer.*;
import org.omg.PortableServer.POA;
import java.util.concurrent.ThreadLocalRandom;

/**
 * RNGServant class
 * Provides a randomly generated number in the range [1, 100].
 * There is a delay from the call to when the number is delayed in order
 * to demonstrate deferred synchronous and asynchronous request type in CORBA.
 * The delay is set to be between 4 and 10 seconds, randomly created.
*/
class RNGServant extends RNGPOA
{
	private ORB orb;

	public void setORB(ORB _orb)
	{
		this.orb = _orb;
	}

	public int getRandomNumber()
	{
		System.out.println("Received a deferred request from a client");
		System.out.println("Generating number..");

		// Randomly create the sleep interval
		int sleepTime = ThreadLocalRandom.current().nextInt(4000, 10000);
		try {
			Thread.sleep(sleepTime);
		} catch (InterruptedException ie) {
			System.out.println("DelayedRNG interrupted when attempting to sleep");
		}

		// Return the randomly generated value
		int val = ThreadLocalRandom.current().nextInt(1, 100);
		System.out.println("Generated value: " + val);
		return val;
	}

	public void getRandomNumberAsync(SyncCallback ref)
	{
		System.out.println("Received an asynchronous request from client: ");
		System.out.println(ref);
		System.out.println("Generating number..");

		// Randomly create the sleep interval
		int sleepTime = ThreadLocalRandom.current().nextInt(4000, 10000);
		try {
			Thread.sleep(sleepTime);
		} catch (InterruptedException ie) {
			System.out.println("DelayedRNG interrupted when attempting to sleep");
		}

		// Generate value and send callback
		int val = ThreadLocalRandom.current().nextInt(1, 100);
		System.out.println("Generated value: " + val);
		ref.callback(val);
		System.out.println("Sent callback to client");
	}
}

/**
 * SyncServer class
 * This server program allows clients to connect to the server using the ORB to
 * retrieve a randomly generated number via the RNGServant class.
 * 
*/
public class SyncServer
{
	public static void main(String[] args)
	{
		try
		{
			ORB orb = ORB.init(args, null);
			// Get a reference to the root POA and activate the POA manager
			POA rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
			rootpoa.the_POAManager().activate();


			RNGServant RNGRef = new RNGServant(); // Create servant

			// Get an object reference for the servant
			org.omg.CORBA.Object ref = rootpoa.servant_to_reference(RNGRef);
			RNG hRef = RNGHelper.narrow(ref);

			// Get root Naming Service context
			org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
		
			NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
			// Bind the Server reference into the Naming Context
			NameComponent nc = new NameComponent("Sync", "");
			NameComponent[] path = {nc};
			ncRef.rebind(path, hRef);

			System.out.println("Server waiting to be called ...");
			orb.run(); // wait for client invocations
		}
		catch (Exception e)
		{
			System.out.println("Error: " + e);
		}
	}
}