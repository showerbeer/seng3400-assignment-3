# Assignment 3 for SENG3400 at The University of Newcastle. #

Introduction to synchronisation in network apps
* Demonstrates deferred synchronous and asynchronous request types using an ORB ([CORBA](https://docs.oracle.com/javase/7/docs/api/org/omg/CORBA/ORB.html))

### To run ###

1: Generate and compile Server and Client java files from IDL:

```
#!console

idlj -fall sync.idl
javac SyncApp\*.java
javac *.java
```
2: Start the ORB

```
#!console

start orbd -ORBInitialPort 2015
```

3: Run server

```
#!console

start java SyncServer  -ORBInitialHost localhost -ORBInitialPort 2015
```

4: Run client	
For deferred synchhronous request type:

```
#!console

java SyncClient -ORBInitialHost localhost -ORBInitialPort 2015 deferred
```

For asynchronous request type

```
#!console

java SyncClient -ORBInitialHost localhost -ORBInitialPort 2015 asynchronous
```